<nav class="${nav_css_class}" id="navigation" role="navigation">
<div class="idioma idiomaMobile" style="display:none;">
			
	<ul>
		<li id="languageES" class="es"><a id="lang_es" class="lang_es" href="#" onfocus="replaceUrl('/es','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_es','0')" onmouseenter="replaceUrl('/es','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_es','1')" target="_self">ES</a>
		</li>
		<div class="dottedSeparator"><span></span><span></span><span></span><span></span><span></span></div>
		<li id="languageEU" class="eu"><a id="lang_eu" class="lang_eu" href="#" onfocus="replaceUrl('/eu','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_eu','0')" onmouseenter="replaceUrl('/eu','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_eu','1')" target="_self">EU</a>
		</li>
		<div class="dottedSeparator"><span></span><span></span><span></span><span></span><span></span></div>
		<li id="languageEN" class="en"><a id="lang_en" class="lang_en" href="#" onfocus="replaceUrl('/en','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_en','0')" onmouseenter="replaceUrl('/en','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_en','1')" target="_self">EN</a>

		</li>
	</ul>
</div>

	<h1 class="hide-accessible"><@liferay.language key="navigation" /></h1>

	<ul aria-label="<@liferay.language key="site-pages" />" role="menubar">
		<#list nav_items as nav_item>
			<#assign
				nav_item_attr_has_popup = ""
				nav_item_attr_selected = ""
				nav_item_css_class = ""
				nav_item_layout = nav_item.getLayout()
			/>

			<#if nav_item.isSelected()>
				<#assign
					nav_item_attr_has_popup = "aria-haspopup='true'"
					nav_item_attr_selected = "aria-selected='true'"
					nav_item_css_class = "selected"
				/>
			</#if>

			<li ${nav_item_attr_selected} class="${nav_item_css_class}" id="layout_${nav_item.getLayoutId()}" role="presentation">
				<a aria-labelledby="layout_${nav_item.getLayoutId()}" ${nav_item_attr_has_popup} href="${nav_item.getURL()}" ${nav_item.getTarget()} role="menuitem"><span><@liferay_theme["layout-icon"] layout=nav_item_layout /> ${nav_item.getName()}</span></a>

				<#if nav_item.hasChildren()>
					<ul class="child-menu" role="menu" style="display:none;">
						<#list nav_item.getChildren() as nav_child>
							<#assign
								nav_child_attr_selected = ""
								nav_child_css_class = ""
							/>

							<#if nav_item.isSelected()>
								<#assign
									nav_child_attr_selected = "aria-selected='true'"
									nav_child_css_class = "selected"
								/>
							</#if>

							<li ${nav_child_attr_selected} class="${nav_child_css_class}" id="layout_${nav_child.getLayoutId()}" role="presentation">
								<a aria-labelledby="layout_${nav_child.getLayoutId()}" href="${nav_child.getURL()}" ${nav_child.getTarget()} role="menuitem">${nav_child.getName()}</a>
							</li>
						</#list>
					</ul>
				</#if>
			</li>
		</#list>
	</ul>
</nav>