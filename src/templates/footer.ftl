<#assign VOID = freeMarkerPortletPreferences.setValue("portletSetupPortletDecoratorId", "barebone") />    
<#assign theme_groupID = htmlUtil.escape(theme_display.getCompanyGroupId()?string) /> 
<#assign VOID = freeMarkerPortletPreferences.setValue("groupId", '${group_id}') />    
<#assign VOID = freeMarkerPortletPreferences.setValue("articleId", '${footerID}') />   
 
<@liferay_portlet["runtime"]        
    defaultPreferences="${freeMarkerPortletPreferences}"        
    portletProviderAction=portletProviderAction.VIEW        
    instanceId="footerContent2"        
    portletName="com_liferay_journal_content_web_portlet_JournalContentPortlet" />        
 
${freeMarkerPortletPreferences.reset()}



