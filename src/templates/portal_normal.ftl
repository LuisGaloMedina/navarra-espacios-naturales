<!DOCTYPE html>

<#include init />

<html class="${root_css_class} ${w3c_language_id}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
	<title>${the_title} - ${company_name}</title>

	<meta content="initial-scale=1.0, width=device-width" name="viewport" />

	<@liferay_util["include"] page=top_head_include />




	<script> define._amd = define.amd; define.amd = false;</script>
	<@liferay.js file_name="${javascript_folder}/jquery-1.12.4.min.js"/>
	<@liferay.js file_name="${javascript_folder}/jquery.mCustomScrollbar.concat.min.js"/>
	<@liferay.js file_name="${javascript_folder}/owl.carousel.min.js"/>
	<@liferay.js file_name="${javascript_folder}/plyr.js"/>

	<@liferay.js file_name="${javascript_folder}/custom.js"/>
	<script>
	(function($){
			$(window).on("load",function(){
				
				$(".aperturaContenedorPrincipal .aperturaParrafo").mCustomScrollbar({
					theme:"inset-2-dark"
				});
				

				
			});
		})(jQuery);
	</script>
	<script> define.amd = define._amd;</script>


	



</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<div class="container-fluid" id="wrapper">
	<div class="wrapperOverlay"></div>
	<header id="banner" role="banner">

		<div id="heading">
			<div class="hamburguesa" style="display: none;"><div class="c1"></div><div class="c2"></div><div class="c3"></div></div>
			<h1 class="site-title">
				<a class="${logo_css_class}" href="${site_default_url}" title="<@liferay.language_format arguments="${site_name}" key="go-to-x" />">



					<span class="logoVector"> <span class="logoTricks"> <span class="a1"></span> <span class="a2"></span> <span class="a3"></span> <span class="a4"></span> <span class="a5"></span> <span class="a6"></span> <span class="a7"></span> <span class="a8"></span> <span class="a9"></span> </span> <span class="logoName">Navarra.es</span> </span>
				</a>

				
			</h1>
		</div>

		<#if has_navigation && is_setup_complete>
			<#include "${full_templates_path}/navigation.ftl" />
		</#if>

		<div class="recursivas">
					<div class="idioma">
			
						<ul>
							<li id="languageES" class="es"><a id="lang_es2" class="lang_es" href="#" onfocus="replaceUrl('/es','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_es2','0')" onmouseenter="replaceUrl('/es','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_es','1')" target="_self">ES</a>
							</li>
							<div class="dottedSeparator"><span></span><span></span><span></span><span></span><span></span></div>
							<li id="languageEU" class="eu"><a id="lang_eu2" class="lang_eu" href="#" onfocus="replaceUrl('/eu','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_eu2','0')" onmouseenter="replaceUrl('/eu','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_eu','1')" target="_self">EU</a>
							</li>
							<div class="dottedSeparator"><span></span><span></span><span></span><span></span><span></span></div>
							<li id="languageEN" class="en"><a id="lang_en2" class="lang_en" href="#" onfocus="replaceUrl('/en','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_en2','0')" onmouseenter="replaceUrl('/en','${themeDisplay.getPortalURL()}','${themeDisplay.getURLCurrent()}','lang_en','1')" target="_self">EN</a>

							</li>
						</ul>
					</div>
					

					<script>
						function replaceUrl(language, portalUrl, urlToReplace, linkId,type){



							var url=urlToReplace.toLowerCase();
							if ((url.indexOf("/eu/")>=0 || url.indexOf("/en/")>=0 ) || url.indexOf("/es/")>=0){




								url=url.replace("/eu/",language.toLowerCase()+"/");
								url=url.replace("/en/",language.toLowerCase()+"/");
								url=url.replace("/es/",language.toLowerCase()+"/");

							


							}
							else{
								url=portalUrl.toLowerCase()+language.toLowerCase()+url;
								
							}

							
								$("#"+linkId).attr("href",url);
						
						}
					</script>

					<div class="circleSeparator"><span></span></div>
					<div class="mapaHome"><a href="${site_default_url}?mapa=mapa"><i class="fa fa-map-o" aria-hidden="true"></i></a></div>
					<div class="circleSeparator"><span></span></div>
					<div class="buscador" style="display:none;"><div class="closeSearchMobile"></div><@liferay.search /></div>
					<div class="buscadorIcon" style=""><span class="icon-magnifier icons"></span></div>
					

				</div>

		


	</header>

	<section id="content">
		<h1 class="hide-accessible">${the_title}</h1>

		<nav id="breadcrumbs">
			<@liferay.breadcrumbs />
		</nav>

		<#if selectable>
			<@liferay_util["include"] page=content_include />
		<#else>
			${portletDisplay.recycle()}

			${portletDisplay.setTitle(the_title)}

			<@liferay_theme["wrap-portlet"] page="portlet.ftl">
				<@liferay_util["include"] page=content_include />
			</@>
		</#if>
	</section>

	<footer id="footer" role="contentinfo">
		 <#include "${full_templates_path}/footer.ftl" />
	</footer>
</div>

<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

<!-- inject:js -->
<!-- endinject -->

</body>

</html>