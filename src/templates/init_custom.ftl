<#--
This file allows you to override and define new FreeMarker variables.
-->

<#assign js_custom_file = htmlUtil.escape(portalUtil.getStaticResourceURL(request, "${javascript_folder}/custom.js")) />
<#assign footerID = getterUtil.getString(theme_settings["FooterID"])>